import Foundation

let DIAL_TIMEOUT_INTERVAL = 4.0
let NSURLCONNECTION_TIMEOUT_IN_SECONDS = 15

extension URLRequest {
  init(with apiCall: Api) {
    
    self.init(url: URL(with: apiCall) as URL,
              cachePolicy: .reloadIgnoringLocalCacheData,
              timeoutInterval: TimeInterval(NSURLCONNECTION_TIMEOUT_IN_SECONDS))
    
    httpMethod = apiCall.httpMethod.rawValue
    if case .wav(let data) = apiCall.bodyContentType {
      addValue("audio/wav", forHTTPHeaderField: "content-type")
      httpBody = data as Data
    }
    else {
      // do we need content type for GET?
      addValue("application/json", forHTTPHeaderField: "Content-Type")
      if case .JSON(let json) = apiCall.bodyContentType {
        httpBody = json
      }
    }
  }
}
