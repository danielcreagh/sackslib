import Foundation

struct NetworkConfig {
  static let serverHostName         = "api.nasa.gov"
  static let serverProtocol         = "https"
  static let serverPortNumber       = 443
  static let apiKey                 = "/* get your own from https://api.nasa.gov/index.html#apply-for-an-api-key */"
}
