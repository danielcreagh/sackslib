import Foundation

extension NetworkConfig {
  static let baseUrl = "\(NetworkConfig.serverProtocol)://\(NetworkConfig.serverHostName):\(NetworkConfig.serverPortNumber)"
}
