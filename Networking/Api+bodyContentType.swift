import Foundation

enum HttpBodyContentType {
  case wav(NSData)
  case JSON(Data?)
  case noContent
}

extension Api {
  var bodyContentType: HttpBodyContentType {
    return .noContent
  }
}
