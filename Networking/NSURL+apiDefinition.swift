import Foundation

enum HttpMethod: String {
  case post   = "POST"
  case get    = "GET"
  case put    = "PUT"
  case delete = "DELETE"
}

extension URL {
  init(with apiDefinition: Api) {
    self.init(string: apiDefinition.urlString)!
  }
}
