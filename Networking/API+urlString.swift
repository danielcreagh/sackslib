import Foundation

extension Api {
  var urlString: URLString {
    return "\(NetworkConfig.baseUrl)/neo/rest/v1/feed?\(startAndEndDateString(from:Date()))&api_key=\(NetworkConfig.apiKey)"
  }
}

func startAndEndDateString(from date:Date) -> String {
  
  let calendar = Calendar.current
  let year = calendar.component(.year, from: date)
  let month = calendar.component(.month, from: date)
  let day = calendar.component(.day, from: date)
  
  let monthString = month < 10 ? "0\(month)" : "\(month)"
  let dayString = day < 10 ? "0\(day)" : "\(day)"
  
  let aWeekInTheFuture = calendar.date(byAdding: .day, value: 7, to: date)
  
  let yearInFuture = calendar.component(.year, from: aWeekInTheFuture!)
  let monthInFuture = calendar.component(.month, from: aWeekInTheFuture!)
  let dayInFuture = calendar.component(.day, from: aWeekInTheFuture!)
  
  let futureMonthString = monthInFuture < 10 ? "0\(monthInFuture)" : "\(monthInFuture)"
  let futureDayString = dayInFuture < 10 ? "0\(dayInFuture)" : "\(dayInFuture)"
  
  return "start_date=\(year)-\(monthString)-\(dayString)&end_date=\(yearInFuture)-\(futureMonthString)-\(futureDayString)"
}

