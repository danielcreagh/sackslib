import Foundation

extension Api {
  var httpMethod: HttpMethod {
    return .get
  }
}
