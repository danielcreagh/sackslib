import Foundation

enum Response<T> {
  case success(T)
  case error(ErrorType)
}

struct EmptyResponseBody: Decodable { }

struct NetworkSession {
  @discardableResult init<T>(with apiCall: Api, responseHandler: @escaping (Response<T>) -> Void) where T: Decodable {
    let request = URLRequest(with: apiCall)
    let session = URLSession(configuration: URLSessionConfiguration.default)
    session.dataTask(with: request) {
      (data: Data?, response: URLResponse?, error: Error?) in
    
      if let networkServiceError = NetworkServiceError(with: error)?.type {
        responseHandler(.error(networkServiceError))
        return
      }
      if let responseError = NetworkServiceError(with: response)?.type {
        responseHandler(.error(responseError))
        return
      }
      if T.self == EmptyResponseBody.self {
        responseHandler(.success(EmptyResponseBody() as! T))
        return
      }
      guard let data = data else {
        responseHandler(.error(.invalidResponse))
        return
      }
      do {
        let responseBody = try JSONDecoder().decode(T.self, from: data)
        responseHandler(.success(responseBody))
      }
      catch DecodingError.keyNotFound(let key) {
        responseHandler(.error(.parsing("key not found: \(key.0.stringValue)")))
      }
      catch DecodingError.valueNotFound(let key) {
        responseHandler(.error(.parsing("value not found for: \(key.0)")))
      }
      catch {
        responseHandler(.error(.parsing("\(error)")))
      }
    }.resume()
  }
}
