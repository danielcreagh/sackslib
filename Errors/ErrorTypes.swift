import Foundation

enum ErrorType {
  case noInternetAccess
  case authenticationFailed
  case serverError
  case updateNeeded
  case invalidRequest
  case conflict
  case unknown
  case invalidResponse
  case timeOut
  case noNetwork
  case parsing(String)
  case custom(String)
  case passwordChanged
  case passwordChangeMismatch
  case passwordChangeError
}

struct NetworkServiceError {  
  var type: ErrorType?
  
  init?(with error: Error?) {
    guard let error = error else { return nil }
    switch error._code {
    case URLError.timedOut.rawValue:
      type = .timeOut
    case URLError.cannotConnectToHost.rawValue:
      type = .serverError
    default:
      type = .unknown
    }
  }
  
  init?(with urlResponse: URLResponse?) {
    guard let response = urlResponse as? HTTPURLResponse else { return nil }
    let responseCode = response.statusCode
    if responseCode < 300 && responseCode >= 200 { return nil }
    type = evaluate(status: responseCode)
  }
}

func evaluate(status code: Int) -> ErrorType {
  switch code {
  case 401: return .authenticationFailed
  case 404: return .invalidRequest
  case 409: return .conflict
  case 500: return .serverError
  case 502: return .serverError
  case 510: return .updateNeeded
  default:  return .unknown
  }
}
