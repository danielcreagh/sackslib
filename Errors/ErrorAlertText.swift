import Foundation

struct ErrorAlertText {
  let title: String
  let body: String
  let cancelButton: String
  
  init(title: String, body: String, cancelButton: String = "OK") {
    self.title = title
    self.body = body
    self.cancelButton = cancelButton
  }
}
